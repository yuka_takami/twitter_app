class TopsController < ApplicationController
  def main
     if session[:login_uid] == nil
       render 'login'
     else
       render 'main'
     end
  end
  
  def login
     user = User.find_by(uid: params[:uid])
    if user && user.pass == params[:pass]
      session[:login_uid] = params[:uid]
	    redirect_to tweets_path
    else
      render 'error'
    end
  end
  
  def logout
    session[:login_uid] = nil
    redirect_to tops_main_path
  end
end
