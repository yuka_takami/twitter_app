class Tweet < ActiveRecord::Base
  validates :message, presence: true, length: {maximum: 140}
  validates :user_id, presence: true
  
  belongs_to :user
  has_many :likes, dependent: :destroy
  has_many :like_users, source: :user, through: :likes
  
  
  def like(user)
    likes.create(user_id: user.id) #tweet_idが自動補完される
  end
  
  def unlike(user)
    likes.find_by(user_id: user.id).destroy
  end
  
  def liked?(user)
    like_users.include?(user)
  end
end
