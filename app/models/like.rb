class Like < ActiveRecord::Base
  validates :user_id, presence: true
  validates :tweet_id, presence: true
  
  belongs_to :user
  belongs_to :tweet
end
