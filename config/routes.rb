Rails.application.routes.draw do
  resources :users, only:[:index, :new, :create, :destroy]
  
  resources :tweets
  
  resources :likes, only: [:create, :destroy]
  
  get 'tops/main'
  post 'tops/login'
  get 'tops/logout'
  root 'tops#main'
end
